import 'dart:async';

import 'package:flutter/services.dart';

typedef SelectNotiCallback = void Function(dynamic msg);

/// 融云推送处理器
class WpNotificationHandler {
  static const MethodChannel _channel =
      const MethodChannel('com.mason.wooplus/wp_notification_handler');

  /// 点击"推送"回调
  SelectNotiCallback? _onSelectNotification;

  static final WpNotificationHandler _instance = WpNotificationHandler._();

  factory WpNotificationHandler() => _instance;

  WpNotificationHandler._();

  /// 初始化插件
  void initialize({
    SelectNotiCallback? onSelectNotification,
  }) {
    _onSelectNotification = onSelectNotification;
    _channel.setMethodCallHandler(_handleMethod);
    _channel.invokeMethod('initialize');
  }

  /// 获取初始消息【在Flutter未启动时，点击推送会将推送消息缓存在本地，待Flutter启动好之后直
  /// 接获取该消息即可】
  Future<dynamic> getInitialMessage() async {
    dynamic result = await _channel.invokeMethod('getInitialMessage');
    return result;
  }

  Future<void> _handleMethod(MethodCall call) async {
    switch (call.method) {
      case 'selectNotification':
        _onSelectNotification?.call(call.arguments);
        break;
      default:
        return await Future<void>.error('Method not defined');
    }
  }
}
