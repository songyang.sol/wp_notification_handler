#import "WpNotificationHandlerPlugin.h"
#import <UserNotifications/UserNotifications.h>

@implementation WpNotificationHandlerPlugin {
    bool _initialized;
    bool _launchingAppFromNotification;
    id _launchPayload;
    NSObject<FlutterPluginRegistrar> *_registrar;
    FlutterMethodChannel *_channel;
}

+ (void)registerWithRegistrar:(NSObject<FlutterPluginRegistrar>*)registrar {
    FlutterMethodChannel* channel = [FlutterMethodChannel
                                     methodChannelWithName:@"com.mason.wooplus/wp_notification_handler"
                                     binaryMessenger:[registrar messenger]];
    WpNotificationHandlerPlugin* instance = [[WpNotificationHandlerPlugin alloc] initWithChannel:channel registrar:registrar];
    [registrar addApplicationDelegate:instance];
    [registrar addMethodCallDelegate:instance channel:channel];
}

- (instancetype)initWithChannel:(FlutterMethodChannel *)channel
                      registrar:(NSObject<FlutterPluginRegistrar> *)registrar {
    self = [super init];
    
    if (self) {
        _channel = channel;
        _registrar = registrar;
    }
    
    return self;
}


- (void)handleMethodCall:(FlutterMethodCall*)call result:(FlutterResult)result {
    if ([@"initialize" isEqualToString:call.method]) {
        [self initialize:call.arguments result:result];
    } else if ([@"getInitialMessage"
                isEqualToString:call.method]) {
        
        result(_launchPayload);
        _launchPayload = nil;
    }else {
        result(FlutterMethodNotImplemented);
    }
}


- (void)initialize:(NSDictionary *_Nonnull)arguments
            result:(FlutterResult _Nonnull)result {
    
    _initialized = true;
}



- (void)userNotificationCenter:(UNUserNotificationCenter *)center
didReceiveNotificationResponse:(UNNotificationResponse *)response
         withCompletionHandler:(void (^)(void))completionHandler
NS_AVAILABLE_IOS(10.0) {
    
    NSDictionary *userInfo =
    (NSDictionary *)response.notification.request.content.userInfo;
    if (_initialized) {
        [self handleSelectNotification:userInfo];
    } else {
        _launchPayload = userInfo;
    }
    completionHandler();
}

- (void)handleSelectNotification:(NSDictionary *)payload {
    [_channel invokeMethod:@"selectNotification" arguments:payload];
}

@end
