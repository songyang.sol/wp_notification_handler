package com.example.wp_notification_handler;

import android.app.Activity;
import android.content.Intent;
import android.util.Log;

import java.util.HashMap;
import java.util.Map;

import androidx.annotation.NonNull;
import io.flutter.embedding.engine.plugins.FlutterPlugin;
import io.flutter.embedding.engine.plugins.activity.ActivityAware;
import io.flutter.embedding.engine.plugins.activity.ActivityPluginBinding;
import io.flutter.plugin.common.MethodCall;
import io.flutter.plugin.common.MethodChannel;
import io.flutter.plugin.common.PluginRegistry;

/**
 * 融云推送处理器
 */
public class WpNotificationHandlerPlugin implements FlutterPlugin, MethodChannel.MethodCallHandler, ActivityAware, PluginRegistry.NewIntentListener {
    private static final String RC_PUSH_TYPE_KEY = "push_type";
    private static final String RC_PUSH_CONTENT_KEY = "push_content";
    private static final String RC_PUSH_TYPE_VALUE = "RC_PUSH";
    private MethodChannel channel;
    private Activity activity;
    private String initPushContent;

    @Override
    public void onAttachedToEngine(@NonNull FlutterPluginBinding binding) {
        channel = new MethodChannel(binding.getBinaryMessenger(), "com.mason.wooplus/wp_notification_handler");
        channel.setMethodCallHandler(this);
    }

    @Override
    public void onDetachedFromEngine(@NonNull FlutterPluginBinding binding) {
        channel.setMethodCallHandler(null);
    }

    @Override
    public void onMethodCall(@NonNull MethodCall call, @NonNull MethodChannel.Result result) {
        switch (call.method) {
            case "initialize":
                result.success(true);
                break;
            case "getInitialMessage":
                getInitialMessage(result);
                break;
            default:
                result.notImplemented();
                return;
        }
    }

    /**
     * 获取初始推送内容
     *
     * @param result
     */
    private void getInitialMessage(MethodChannel.Result result) {
        // 1.如果有初始化推送内容
        if (initPushContent != null) {
            Map<String, Object> tempResult = messageToMap(initPushContent);
            initPushContent = null;
            result.success(tempResult);
            return;
        }
        // 2.如果没有Activity，则返回null
        if (activity == null) {
            result.success(null);
            return;
        }
        // 3.没有初始推送内容，则从Intent中现取
        Intent intent = activity.getIntent();
        if (intent == null || intent.getExtras() == null) {
            result.success(null);
            return;
        }
        String pushType = intent.getExtras().getString(RC_PUSH_TYPE_KEY);
        if (pushType == null || !pushType.equals(RC_PUSH_TYPE_VALUE)) {
            result.success(null);
            return;
        }
        String pushContent = intent.getExtras().getString(RC_PUSH_CONTENT_KEY);
        if (pushContent == null || pushContent.isEmpty()) {
            result.success(null);
            return;
        }
        // 4.返回结果
        result.success(messageToMap(pushContent));
    }

    @Override
    public void onAttachedToActivity(@NonNull ActivityPluginBinding binding) {
        binding.addOnNewIntentListener(this);
        activity = binding.getActivity();
        if (activity.getIntent() != null && activity.getIntent().getExtras() != null) {
            if ((activity.getIntent().getFlags() & Intent.FLAG_ACTIVITY_LAUNCHED_FROM_HISTORY)
                    != Intent.FLAG_ACTIVITY_LAUNCHED_FROM_HISTORY) {
                onNewIntent(activity.getIntent());
            }
        }
    }

    @Override
    public void onDetachedFromActivityForConfigChanges() {
        activity = null;
    }

    @Override
    public void onReattachedToActivityForConfigChanges(@NonNull ActivityPluginBinding binding) {
        binding.addOnNewIntentListener(this);
        activity = binding.getActivity();
    }

    @Override
    public void onDetachedFromActivity() {
        activity = null;
    }

    @Override
    public boolean onNewIntent(Intent intent) {
        // 0. 如果Intent无内容，则忽略
        if (intent == null || intent.getExtras() == null) {
            return false;
        }
        // 1.如果不是融云推送，则不处理
        String pushType = intent.getExtras().getString(RC_PUSH_TYPE_KEY);
        if (pushType == null || !pushType.equals(RC_PUSH_TYPE_VALUE)) {
            return false;
        }
        // 2.如果融云推送无内容，则不处理
        String pushContent = intent.getExtras().getString(RC_PUSH_CONTENT_KEY);
        if (pushContent == null || pushContent.isEmpty()) {
            return false;
        }
        // 3.将推送内容缓存
        initPushContent = pushContent;
        // 4.通知Flutter层处理推送内容
        channel.invokeMethod("selectNotification", messageToMap(pushContent));
        activity.setIntent(intent);
        return true;
    }

    /**
     * 将推送内容转成Map
     *
     * @param pushContent
     * @return
     */
    private Map<String, Object> messageToMap(String pushContent) {
        Map<String, Object> result = new HashMap<>();
        result.put("appData", pushContent);
        return result;
    }
}
